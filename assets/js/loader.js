function includeHTML() {
  $('[data-html-src]').each((index ,ele)=>{
    $.get($(ele).attr('data-html-src') , (data)=> {
      $(ele).html(data)
      window.scrollTo(0,0)
      ele.removeAttribute('data-html-src')
    })
  })
}

includeHTML()

// function includeHTML() {
//   document.querySelectorAll('[data-html-src]').forEach(async (ele , index) =>{
//     const elementAttribute = ele.getAttribute('data-html-src')
//     const res = await fetch(elementAttribute);
//     const data = await res.text()
//     ele.innerHTML = data
//     ele.removeAttribute('data-html-src')
//   })
// }